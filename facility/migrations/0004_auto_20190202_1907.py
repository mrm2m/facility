# Generated by Django 2.1.5 on 2019-02-02 19:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('facility', '0003_equipment'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipment',
            name='environment',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='facility.Location'),
            preserve_default=False,
        ),
    ]
